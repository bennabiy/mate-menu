# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Atilla Öntaş <tarakbumba@gmail.com>, 2015
# mauron, 2015
# mauron, 2015
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-11 22:31+0000\n"
"PO-Revision-Date: 2016-03-06 00:23+0000\n"
"Last-Translator: Atilla Öntaş <tarakbumba@gmail.com>\n"
"Language-Team: Turkish (http://www.transifex.com/mate/MATE/language/tr/)\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "Menü"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:254
msgid "Couldn't load plugin:"
msgstr "Eklenti yüklenemedi:"

#: ../lib/mate-menu.py:326
msgid "Couldn't initialize plugin"
msgstr "Eklenti başlatılamadı"

#: ../lib/mate-menu.py:715
msgid "Advanced MATE Menu"
msgstr "Gelişmiş MATE Menüsü"

#: ../lib/mate-menu.py:798
msgid "Preferences"
msgstr "Tercihler"

#: ../lib/mate-menu.py:801
msgid "Edit menu"
msgstr "Menüyü düzenle"

#: ../lib/mate-menu.py:804
msgid "Reload plugins"
msgstr "Eklentileri tekrar yükle"

#: ../lib/mate-menu.py:807
msgid "About"
msgstr "Hakkında"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "Menü tercihleri"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr "Daima favoriler sekmesi ile başla"

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "Düğme simgesini göster"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "Özel renk kullan"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr "Son kullanılan belgeler eklentisini göster"

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr "Uygulamalar eklentisini göster"

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr "Sistem eklentisini göster"

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr "Yerler eklentisini göster"

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr "Uygulama yorumlarını göster"

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "Kategori simgelerini göster"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr "Üzerinde tut"

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr "Son kategori ya da aramayı hatırla"

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr "İsim ile alelade ismi değiştir"

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "Kenarlık kalınlığı:"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "piksel"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr "Donukluk:"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "Düğme metni:"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "Seçenekler"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:252
msgid "Applications"
msgstr "Uygulamalar"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "Tema"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:249
#: ../mate_menu/plugins/applications.py:250
msgid "Favorites"
msgstr "Favoriler"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "Ana düğme"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr "Eklentiler"

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "Arka plan:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "Başlıklar:"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "Kenarlıklar:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "Tema:"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "Sütun sayısı:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "İkon boyutu:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr "Üzerinde tutma süresi (ms):"

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "Düğme simgesi:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "Arama komutu:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "Yerler"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "Kaydıma Çubuğuna Müsaade Et"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK+ Bookmarks"
msgstr "GTK+ Yer İmlerini Göster"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "Yükseklik:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr "Varsayılan Yerleri Değiştir:"

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "Bilgisayar"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "Ev Klasörü"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "Ağ"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "Masaüstü"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "Çöp kutusu"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "Özel Yerler:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "Sistem"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr "Varsayılan Unsurları Değiştir:"

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:149
#: ../mate_menu/plugins/system_management.py:152
#: ../mate_menu/plugins/system_management.py:155
msgid "Package Manager"
msgstr "Paket Yöneticisi"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:162
msgid "Control Center"
msgstr "Kontrol Merkezi"

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:169
msgid "Terminal"
msgstr "Uçbirim"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:179
msgid "Lock Screen"
msgstr "Ekranı Kilitle"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "Çıkış"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:197
msgid "Quit"
msgstr "Çık"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "Yeri Düzenle"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "Yeni Yer"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "Klasör seç"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr "Klavye kısayolu:"

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr "Resimler"

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "İsim"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "Yol"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr "Masaüstü teması"

#: ../lib/mate-menu-config.py:432 ../lib/mate-menu-config.py:463
msgid "Name:"
msgstr "İsim:"

#: ../lib/mate-menu-config.py:433 ../lib/mate-menu-config.py:464
msgid "Path:"
msgstr "Yol:"

#. i18n
#: ../mate_menu/plugins/applications.py:247
msgid "Search:"
msgstr "Ara:"

#: ../mate_menu/plugins/applications.py:251
msgid "All applications"
msgstr "Tüm uygulamalar"

#: ../mate_menu/plugins/applications.py:618
#, python-format
msgid "Search Google for %s"
msgstr "Google'da %s unsurunu ara"

#: ../mate_menu/plugins/applications.py:625
#, python-format
msgid "Search Wikipedia for %s"
msgstr "Vikipedi'de %s unsurunu ara"

#: ../mate_menu/plugins/applications.py:641
#, python-format
msgid "Lookup %s in Dictionary"
msgstr "Sözlükte %s unsurunu ara"

#: ../mate_menu/plugins/applications.py:648
#, python-format
msgid "Search Computer for %s"
msgstr "Bilgisayarda %s unsurunu ara"

#. i18n
#: ../mate_menu/plugins/applications.py:761
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "Masaüstüne ekle"

#: ../mate_menu/plugins/applications.py:762
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "Panele ekle"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:812
msgid "Insert space"
msgstr "Boşluk ekle"

#: ../mate_menu/plugins/applications.py:765
#: ../mate_menu/plugins/applications.py:813
msgid "Insert separator"
msgstr "Ayırıcı ekle"

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr "Giriş yaptığımda başlat"

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr "Başlat"

#: ../mate_menu/plugins/applications.py:770
msgid "Remove from favorites"
msgstr "Favorilerden kaldır"

#: ../mate_menu/plugins/applications.py:772
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr "Özellikleri düzenle"

#. i18n
#: ../mate_menu/plugins/applications.py:811
msgid "Remove"
msgstr "Kaldır"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr "Favorilerimde göster"

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "Menüden sil"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr "Google'da ara"

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr "Vikipedi'de ara"

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr "Sözlükte ara"

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr "Bilgisayarda ara"

#: ../mate_menu/plugins/applications.py:1332
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""
"Favoriler kaydedilemedi. ~/.config/mate-menu dosyasına yazma iziniz olup "
"olmadığını kontrol edin."

#: ../mate_menu/plugins/applications.py:1532
msgid "All"
msgstr "Hepsi"

#: ../mate_menu/plugins/applications.py:1532
msgid "Show all applications"
msgstr "Tüm uygulamaları göster"

#: ../mate_menu/plugins/system_management.py:159
msgid "Install, remove and upgrade software packages"
msgstr "Yazılım paketlerini kur, kaldır ve güncelle"

#: ../mate_menu/plugins/system_management.py:166
msgid "Configure your system"
msgstr "Sisteminizi yapılandırın"

#: ../mate_menu/plugins/system_management.py:176
msgid "Use the command line"
msgstr "Komut satırını kullan"

#: ../mate_menu/plugins/system_management.py:187
msgid "Requires password to unlock"
msgstr "Kilidi açmak için parola gerekir"

#: ../mate_menu/plugins/system_management.py:190
msgid "Logout"
msgstr "Çıkış"

#: ../mate_menu/plugins/system_management.py:194
msgid "Log out or switch user"
msgstr "Çıkış yap ya da kullanıcı değiştir"

#: ../mate_menu/plugins/system_management.py:201
msgid "Shutdown, restart, suspend or hibernate"
msgstr "Kapat, yeniden başlat, askıya al ya da uyku kipine al"

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr "Bu bilgisayardan tüm yerel ve uzaktaki disklere ve klasörlere göz atın"

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "Kişisel klasörünüzü açın"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr "Yer imi oluşturulmuş ve yerel ağ konumlarını göz at"

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr "Masaüstüne konmuş unsurları tara"

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr "Silinmiş dosyaları tara"

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "Çöp kutusunu boşalt"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "Son kullanılan belgeler"

#: ../mate_menu/keybinding.py:199
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""
"Menüyü açıp kapatacak için yeni bir hızlandırıcı tuş seçmek için tıklayın."

#: ../mate_menu/keybinding.py:200
msgid "Press Escape or click again to cancel the operation.  "
msgstr "İşlemi iptal etmek için Escape tuşuna basın ya da tekrar tıklayın."

#: ../mate_menu/keybinding.py:201
msgid "Press Backspace to clear the existing keybinding."
msgstr "Mevcut kısayolu silmek için geri silme tuşuna basın."

#: ../mate_menu/keybinding.py:214
msgid "Pick an accelerator"
msgstr "Bir hızlandırıcı seçin"

#: ../mate_menu/keybinding.py:267
msgid "<not set>"
msgstr "<ayarlanmamış>"
